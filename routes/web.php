<?php

use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', function () {
    return view('admin.dashboard.index');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'sms'], function () {
    Route::get('/dashboard', 'SmsController@index')->name('sms.dashboard');
    Route::get('/enviados', 'SmsController@enviados')->name('sms.enviados');
    Route::get('/entregues', 'SmsController@entregues')->name('sms.entregues');
    Route::get('/cliques', 'SmsController@cliques')->name('sms.cliques');
    Route::get('/respostas', 'SmsController@respostas')->name('sms.respostas');
    Route::post('/enviados', 'SmsController@buscarEnviados')->name('sms.buscar.enviados');
    Route::post('/entregues', 'SmsController@buscarEntregues')->name('sms.buscar.entregues');
    Route::post('/cliques', 'SmsController@buscarCliques')->name('sms.buscar.cliques');
    Route::post('/respostas', 'SmsController@buscarRespostas')->name('sms.buscar.respostas');
    Route::get('/extrato', 'SmsController@extrato')->name('sms.extrato');
});


Route::group(['prefix' => 'ura'], function () {
    Route::get('/dashboard', 'UraController@index')->name('ura.dashboard');
    Route::get('/ligacoes', 'UraController@ligacoes')->name('ura.ligacoes');
    Route::post('/ligacoes', 'UraController@buscarLigacoes')->name('ura.buscar.ligacoes');
    Route::get('/atendidas', 'UraController@atendidas')->name('ura.atendidas');
    Route::post('/atendidas', 'UraController@buscarAtendidas')->name('ura.buscar.atendidas');
    Route::get('/completa', 'UraController@completa')->name('ura.completa');
    Route::post('/completa', 'UraController@buscarCompleta')->name('ura.buscar.completa');
    Route::get('/parcial', 'UraController@parcial')->name('ura.parcial');
    Route::post('/parcial', 'UraController@buscarParcial')->name('ura.buscar.parcial');
    Route::get('/extrato', 'UraController@extrato')->name('ura.extrato');
});
