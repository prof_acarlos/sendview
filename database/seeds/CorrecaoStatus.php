<?php

use Illuminate\Database\Seeder;
use App\Models\Destinatario;
use App\Models\Status;

class CorrecaoStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        Destinatario::whereMensagem('OCUP')->whereTipo(2)->whereStatus(0)->update([
            'status' => Status::TELEFONE_OCUPADO
        ]);
        Destinatario::whereMensagem('NATN')->whereTipo(2)->whereStatus(0)->update([
            'status' => Status::TELEFONE_NAO_ATENDE
        ]);
        Destinatario::whereMensagem('ENTR')->whereTipo(2)->where('status', '<', 20)->update([
            'status' => Status::TELEFONE_NAO_ATENDE
        ]);
        Destinatario::whereMensagem('HUMA')->whereTipo(2)->whereStatus(0)->update([
            'status' => Status::TELEFONE_NAO_ATENDE
        ]);
        Destinatario::whereMensagem('DISO')->whereTipo(2)->whereStatus(0)->update([
            'status' => Status::TELEFONE_NAO_ATENDE
        ]);
        Destinatario::whereMensagem('ENTR')->whereTipo(2)->whereBetween('status' , [20 ,50 ])->update([
            'status' => Status::LIGACAO_ATENDIDA
        ]);
        Destinatario::whereMensagem('ENTR')->whereTipo(2)->where('status' , '>', 50 )->update([
            'status' => Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE
        ]);
    }
}
