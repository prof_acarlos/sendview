<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;
use App\User;
use App\Models\Destinatario;

class MassiveSms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cliente = null;
        try {
            $cliente  = Cliente::create(
                ['nome' => 'Jumps']
            );
        } catch (\Throwable $th) {
            $cliente = Cliente::find(1);
        }

        try {
            User::create([
                'name' => 'Antonio',
                'email' => 'acarlos.os@hotmail.com',
                'password' => \Hash::make('pedro0409'),
                'cliente_id' => $cliente->id
            ]);
            var_dump('Adicionado Usuário');
        } catch (\Exception $e) {
            User::whereId(1)->whereNull('cliente_id')->update(['cliente_id' => $cliente->id]);
            var_dump('Atualizado Cliente Id');
        }

        $cliente = null;
        try {
            $cliente  = Cliente::create(
                ['nome'=> 'Prefeitura de São Caetano do Sul']
            );
        } catch (\Throwable $th) {
            $cliente = Cliente::find(2);
        }

        try {
            User::create([
                'name' => 'Ricardo',
                'email' => 'ricardo@shoutpubliciade.com.br',
                'password' => \Hash::make('Paulista509'),
                'cliente_id' => $cliente->id
            ]);
            var_dump('Adicionado Usuário');
        } catch (\Exception $e) {
            User::whereEmail('ricardo@shoutpubliciade.com.br')->whereNull('cliente_id')->update(['cliente_id' => $cliente->id]);
            var_dump('Atualizado Cliente Id');
        }

        try {
            User::create([
                'name' => 'Prefeitura',
                'email' => 'scs@shoutpubliciade.com.br',
                'password' => \Hash::make('Campanha2019'),
                'cliente_id' => $cliente->id
            ]);
        } catch (\Exception $e) {
            User::whereEmail('scs@shoutpubliciade.com.br')->whereNull('cliente_id')->update(['cliente_id' => $cliente->id]);
            var_dump('Atualizado Cliente Id');
        }

      

        $cliente = null;
        $nome = '';
        try {
            $nome = 'DIEESE - Departamento Intersindical de Estatística e Estudos Socioeconômicos';
            $cliente  = Cliente::create(
                ['nome'=> $nome]
            );
        } catch (\Throwable $th) {
            $cliente = Cliente::whereNome($nome)->first();
        }

        try {
            User::create([
                'name' => 'Dieese',
                'email' => 'dieese@libris.com.br',
                'password' => \Hash::make('Campanha2019'),
                'cliente_id' => $cliente->id
            ]);
        } catch (\Exception $e) {
            User::whereEmail('dieese@libris.com.br')->whereNull('cliente_id')->update(['cliente_id' => $cliente->id]);
            var_dump('Atualizado Cliente Id');
        }


        // Ajuste SMS Antigo, adicionando cliente_id
        try {
            Destinatario::whereNull('cliente_id')->update(['cliente_id' => 2]);
            var_dump('Atualizado');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        // Ajuste SMS Antigo, adicionando cliente_id
        try {
            Destinatario::whereCampanha(0)->update(['campanha' => 575]);
            var_dump('Atualizado');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

    }
}
