<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new Status();
        $objeto = null;
        foreach($status->obterStatus() as $key => $value){
            $objeto = [
                'codigo' => $key, 
                'mensagem' => $value
            ];
            try {
                Status::create($objeto);
                var_dump('Status: ' . $key . ' , Mensagem: ' . $value . ' Criado');
            } catch (\Exception $e) {
                Status::whereCodigo($objeto['codigo'])->update($objeto);
            }
        }
    }
}
