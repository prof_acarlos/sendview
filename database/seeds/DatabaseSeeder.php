<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(MassiveSms::class);
        $this->call(StatusSeed::class);
        $this->call(CorrecaoStatus::class);
    }
}
