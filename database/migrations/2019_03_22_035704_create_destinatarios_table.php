<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinatariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinatarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_terceiros');
            $table->string('celular')->nullable();
            $table->string('telefone')->nullable();
            $table->string('operadora')->nullable();
            $table->dateTime('enviado')->nullable();
            $table->integer('acesso')->nullable();
            $table->integer('status')->nullable();
            $table->integer('tipo')->nullable();
            $table->text('mensagem')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinatarios');
    }
}
