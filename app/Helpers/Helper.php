<?php

function ativo($menus)
{
    $url = explode('.', Route::currentRouteName());
    if (is_array($menus)) {
        foreach ($menus as $menu) {
            if ($url[0] == $menu) {
                return 'active';
            } else {
                foreach ($url as $u) {
                    if ($u == $menu) {
                        return 'active';
                    }
                }
            }
        }
    } else {
        if ($url[0] == $menus) {
            return 'active';
        } else {
            foreach ($url as $u) {
                if ($u == $menus) {
                    return 'active';
                }
            }
        }
    }
    return '';
}

function mask($val, $mask)
{
    return mascara($val, $mask);
}

function mascara($valor, $mascara)
{
    $texto = '';
    $item = 0;
    for ($i = 0 ; $i < strlen($mascara); $i++) {
        if ($mascara[$i] == '#') {
            if (isset($valor[$item])) {
                $texto .= $valor[$item++];
            }
        } elseif ($mascara[$i] == 'X') {
            $item++;
            if (isset($mascara[$i])) {
                $texto .= $mascara[$i];
            }
        } else {
            if (isset($mascara[$i])) {
                $texto .= $mascara[$i];
            }
        }
    }
    return $texto;
}


function porcentagem($entregues , $ligacoes){
    if($ligacoes){
        return ceil($entregues/$ligacoes*100);
    }
    return 0;
}
