<?php

namespace App\Repository;

use App\Models\Destinatario;
use App\Models\Status;

class UraRepository
{
    protected $user;
    public function model()
    {
        $this->user = auth()->user();
        $cliente = $this->user->cliente;
        if($cliente->id == 1 ){
            return (new Destinatario())->whereTipo(2);
        }
        return (new Destinatario())->whereTipo(2)->whereClienteId($cliente->id);
    }

    public function extrato()
    {
        $totais['Ligações'] = $this->model()->count();
        $totais['Atendidas'] = $this->model()->whereIn('status', [Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE , Status::LIGACAO_ATENDIDA])->count();
        $totais['Ouviram'] = $this->model()->whereStatus(Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE)->count();
        $totais['Ouviram Parcial'] = $this->model()->whereStatus(Status::LIGACAO_ATENDIDA)->count();
        $totais['Ocupado'] = $this->model()->whereStatus(Status::TELEFONE_OCUPADO)->count();
        $totais['Não Atendida'] = $this->model()->whereStatus(Status::TELEFONE_NAO_ATENDE)->count();
        return $totais;
    }

    public function totais()
    {

        $totais['ligacoes'] = $this->model()->count();
        $totais['entregues'] = $this->model()->whereIn('status', [Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE , Status::LIGACAO_ATENDIDA])->count();
        $totais['completa'] = $this->model()->whereStatus(Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE)->count();
        $totais['parcial'] = $this->model()->whereStatus(Status::LIGACAO_ATENDIDA)->count();
        $totais['ocupados'] = $this->model()->whereStatus(Status::TELEFONE_OCUPADO)->count();
        $totais['naoAtendidos'] = $this->model()->whereStatus(Status::TELEFONE_NAO_ATENDE)->count();

        return $totais;
    }

    

    public function ligacoes()
    {
        return $this->model()->paginate(100);
    }

    public function buscarLigacoes($request)
    {
        return $this->model()->where('telefone', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }

    public function atendidas()
    {
        return $this->model()->whereIn('status', [Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE , Status::LIGACAO_ATENDIDA])->paginate(100);
    }

    public function buscarAtendidas($request)
    {
        return $this->model()->whereIn('status', [Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE , Status::LIGACAO_ATENDIDA])->where('telefone', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }

    public function completa()
    {
        return $this->model()->whereStatus(Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE)->paginate(100);
    }

    public function buscarCompleta($request)
    {
        return $this->model()->whereStatus(Status::LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE)->where('telefone', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }
    

    public function parcial()
    {
        return $this->model()->whereStatus(Status::LIGACAO_ATENDIDA)->paginate(100);
    }

    public function buscarParcial($request)
    {
        return $this->model()->whereStatus(Status::LIGACAO_ATENDIDA)->where('telefone', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }
}
