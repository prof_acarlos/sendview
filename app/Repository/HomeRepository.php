<?php

namespace App\Repository;

use App\Models\Destinatario;
use App\User;
use App\Models\Status;

class HomeRepository
{
    protected $user;
    protected $cliente;
    public function sms()
    {
        $this->user = auth()->user();
        $cliente = $this->user->cliente;
        if($cliente->id == 1 ){
            return (new Destinatario())->whereTipo(1);
        }
        return (new Destinatario())->whereTipo(1)->whereClienteId($cliente->id);
    }

    public function extrato()
    {
            if(auth()->user()->cliente_id == 2 ){

                $totais['Enviados'] = $this->sms()->count();
                $totais['Entregues'] = $this->sms()->whereIn('status', [Status::ENVIADO_PARA_DISPARO, Status::ENTREGUE_AO_DESTINATARIO])->count();
                $totais['Cliques'] = $this->sms()->where('acesso', '>', 0)->sum('acesso');
                $totais['Respostas'] = $this->sms()->whereNotNull('mensagem')->count();
            }else{
                $totais['Aguardando confirmação'] = $this->sms()->whereStatus(Status::AGUARDANDO_CONFIRMAÇÃO)->count();
                $totais['Entregue operadora'] = $this->sms()->whereStatus(Status::ENTREGUE_A_OPERADORA)->count();
                $totais['Entregue ao destinatário'] = $this->sms()->whereStatus(Status::ENTREGUE_AO_DESTINATARIO)->count();
                $totais['Não recebido'] = $this->sms()->whereStatus(Status::NAO_RECEBIDO)->count();
                $totais['Entregues'] = $this->sms()->whereIn('status', [ Status::ENTREGUE_AO_DESTINATARIO, Status::ENTREGUE_A_OPERADORA, Status::NAO_RECEBIDO])->count();
            }
    
            return $totais;

        return $totais;
    }

    public function totais()
    {
        $totais['enviados'] = $this->sms()->count();
        $totais['entregues'] = $this->sms()->whereIn('status', [
            Status::ENVIADO_PARA_DISPARO, Status::ENTREGUE_AO_DESTINATARIO
        ])->count();
        $totais['acessos'] = $this->sms()->where('acesso', '>', 0)->sum('acesso');
        $totais['respostas'] = $this->sms()->whereNotNull('mensagem')->count();
        if(! $this->user){
            $this->user = auth()->user();
        }
        $cliente = $this->user->cliente;

        $enviados = \DB::select('select SUBSTRING(enviado, 12,2)  as hora,  count(1) as total from destinatarios where tipo = 1 and cliente_id = ? group by hora;', [$cliente->id]);
        $totais['grafico']['enviados'] = $this->graficoEnviados($enviados);

        $entregues = \DB::select('select SUBSTRING(enviado, 12,2)  as hora,  count(1) as total from destinatarios where status in (? ,?) and tipo = 1 and cliente_id = ? group by hora;' , [
            Status::ENVIADO_PARA_DISPARO, Status::ENTREGUE_AO_DESTINATARIO, $cliente->id
        ]);
        $totais['grafico']['entregues'] = $this->graficoEntregues($entregues);

        $cliques = \DB::select('select SUBSTRING(enviado, 12,2)  as hora,  sum(acesso) as total from destinatarios where acesso > 0 and tipo = 1 and cliente_id = ? group by hora;' , [$cliente->id]);
        $totais['grafico']['cliques'] = $this->graficoCliques($cliques);

        $respostas = \DB::select('select SUBSTRING(enviado, 12,2)  as hora,  count(1) as total from destinatarios where mensagem is not null and tipo = 1 and cliente_id = ? group by hora;' , [$cliente->id]);
        $totais['grafico']['respostas'] = $this->graficoRespostas($respostas);


        return $totais;
    }

    public function graficoEnviados($enviados)
    {
        $label = '';
        $total = '';
        foreach ($enviados as $e) {
            $label .= "'". $e->hora . ":00' ,";
            $total .= "'". $e->total . "' ,";
        }
        $label = rtrim($label, ',');
        $total = rtrim($total, ',');
        $enviados['label'] = $label;
        $enviados['total'] = $total;
        return $enviados;
    }

    public function graficoEntregues($entregues)
    {
        $label = '';
        $total = '';
        foreach ($entregues as $e) {
            $label .= "'". $e->hora . ":00' ,";
            $total .= "'". $e->total . "' ,";
        }
        $label = rtrim($label, ',');
        $total = rtrim($total, ',');
        $entregues['label'] = $label;
        $entregues['total'] = $total;
        return $entregues;
    }

    public function graficoCliques($entregues)
    {
        $label = '';
        $total = '';
        foreach ($entregues as $e) {
            $label .= "'". $e->hora . ":00' ,";
            $total .= "'". $e->total . "' ,";
        }
        $label = rtrim($label, ',');
        $total = rtrim($total, ',');
        $entregues['label'] = $label;
        $entregues['total'] = $total;
        return $entregues;
    }

    public function graficoRespostas($entregues)
    {
        $label = '';
        $total = '';
        foreach ($entregues as $e) {
            $label .= "'". $e->hora . ":00' ,";
            $total .= "'". $e->total . "' ,";
        }
        $label = rtrim($label, ',');
        $total = rtrim($total, ',');
        $entregues['label'] = $label;
        $entregues['total'] = $total;
        return $entregues;
    }

    public function enviados()
    {
        return $this->sms()->paginate(100);
    }

    public function entregues()
    {
        return $this->sms()->whereIn('status', [
            Status::ENVIADO_PARA_DISPARO, Status::ENTREGUE_AO_DESTINATARIO
        ])->paginate(100);
    }

    public function cliques()
    {
        return $this->sms()->where('acesso', '>', 0)->paginate(100);
    }

    public function respostas()
    {
        return $this->sms()->whereNotNull('mensagem')->paginate(7);
    }

    public function buscarEnviados($request)
    {
        return $this->sms()->where('celular', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }

    public function buscarEntregues($request)
    {
        return $this->sms()->whereIn('status', [
            Status::ENVIADO_PARA_DISPARO, Status::ENTREGUE_AO_DESTINATARIO
        ])->where('celular', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }
    public function buscarCliques($request)
    {
        return $this->sms()->where('acesso', '>', 0)->where('celular', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }
    public function buscarRespostas($request)
    {
        return $this->sms()->whereNotNull('mensagem')->where('celular', 'like', '%'. trim($request['table_search'] . '%'))->paginate(100);
    }
}
