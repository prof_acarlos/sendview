<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Destinatario;
use App\Repository\HomeRepository;

class SmsController extends Controller
{
    protected $destinatario;
    protected $homeRepository;
    protected $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Destinatario $destinatario, HomeRepository $homeRepository)
    {
        $this->middleware('auth');
        $this->destinatario = $destinatario;
     
        $this->homeRepository = $homeRepository;
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totais = $this->homeRepository->totais();
        
        return view('admin.sms.index', compact('totais'));
    }

    public function enviados()
    {
        $objetos = $this->homeRepository->enviados();
        return view('admin.sms.enviados', compact('objetos'));
    }
    
    public function entregues()
    {
        $objetos = $this->homeRepository->entregues();
        return view('admin.sms.entregues', compact('objetos'));
    }
    public function cliques()
    {
        $objetos = $this->homeRepository->cliques();
        return view('admin.sms.cliques', compact('objetos'));
    }
    public function respostas()
    {
        $objetos = $this->homeRepository->respostas();
        return view('admin.sms.respostas', compact('objetos'));
    }

    public function buscarEnviados(Request $request)
    {
        $objetos = $this->homeRepository->buscarEnviados($request->all());
        return view('admin.sms.enviados', compact('objetos'));
    }
    public function buscarEntregues(Request $request)
    {
        $objetos = $this->homeRepository->buscarEntregues($request->all());
        return view('admin.sms.entregues', compact('objetos'));
    }
    public function buscarCliques(Request $request)
    {
        $objetos = $this->homeRepository->buscarCliques($request->all());
        return view('admin.sms.cliques', compact('objetos'));
    }
    public function buscarRespostas(Request $request)
    {
        $objetos = $this->homeRepository->buscarRespostas($request->all());
        return view('admin.sms.respostas', compact('objetos'));
    }

    public function extrato()
    {
        $extrato = $this->homeRepository->extrato();
        return view('admin.sms.extrato', compact('extrato'));
    }
}
