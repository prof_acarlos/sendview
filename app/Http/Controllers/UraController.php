<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Destinatario;
use App\Repository\UraRepository;

class UraController extends Controller
{
    protected $destinatario;
    protected $uraRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Destinatario $destinatario, UraRepository $uraRepository)
    {
        $this->middleware('auth');
        $this->destinatario = $destinatario;
        $this->uraRepository = $uraRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totais = $this->uraRepository->totais();
        return view('admin.ura.index', compact('totais'));
    }

    public function ligacoes()
    {
        $objetos = $this->uraRepository->ligacoes();
        return view('admin.ura.ligacoes', compact('objetos'));
    }

    public function buscarLigacoes(Request $request)
    {
        $objetos = $this->uraRepository->buscarLigacoes($request->all());
        return view('admin.ura.ligacoes', compact('objetos'));
    }

    public function atendidas()
    {
        $objetos = $this->uraRepository->atendidas();
        return view('admin.ura.atendidas', compact('objetos'));
    }

    public function buscarAtendidas(Request $request)
    {
        $objetos = $this->uraRepository->buscarAtendidas($request->all());
        return view('admin.ura.atendidas', compact('objetos'));
    }
    public function completa()
    {
        $objetos = $this->uraRepository->completa();
        return view('admin.ura.completa', compact('objetos'));
    }

    public function buscarCompleta(Request $request)
    {
        $objetos = $this->uraRepository->buscarCompleta($request->all());
        return view('admin.ura.completa', compact('objetos'));
    }
    public function parcial()
    {
        $objetos = $this->uraRepository->parcial();
        return view('admin.ura.parcial', compact('objetos'));
    }

    public function buscarParcial(Request $request)
    {
        $objetos = $this->uraRepository->buscarParcial($request->all());
        return view('admin.ura.parcial', compact('objetos'));
    }

    public function extrato()
    {
        $extrato = $this->uraRepository->extrato();
        return view('admin.ura.extrato', compact('extrato'));
    }
}
