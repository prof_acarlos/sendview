<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Cliente extends Model
{
    protected $fillable = [
        'nome'
    ];

    public function users(){
        return $this->hasMany(User::class);
    }
}
