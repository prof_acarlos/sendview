<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';

    const PRONTO_PARA_ENVIO = 0;
    const ENVIADO_PARA_DISPARO = 1;
    const AGUARDANDO_CONFIRMAÇÃO = 2;
    const ENTREGUE_A_OPERADORA = 3;
    const NAO_RECEBIDO = 4;
    const DESTINATARIO_ENVIOU_RESPOSTA = 5;
    const SEM_MENSAGEM = 10;
    const MENSAGEM_MUITO_LONGA = 12;
    const DESTINATARIO_INVALIDO = 13;
    const CELULAR_NAO_INFORMADO = 14;
    const AGENDAMENTO_INVALIDO = 15;
    const ID_COM_MAIS_DE_50_CARACTERES = 16;
    const TELEFONE_OCUPADO = 17;
    const TELEFONE_NAO_ATENDE = 18;
    const LIGACAO_ATENDIDA = 19;
    const LIGACAO_ATENDIDA_MENSAGEM_ENTREGUE = 20;
    const ID_JA_FOI_UTILUZADO_NAS_ULTIMAS_24_HORAS = 80;
    const LOGIN_INVALIDO = 900;
    const LIMIT_DE_CREDITO_ATINGIDO = 990;
    const ERRO_DESCONHECIDO = 999;
    const ERRO = 70;
    const REJEITADO = 71;
    const ENTREGUE_AO_DESTINATARIO = 90;


    protected $fillable = [
        'codigo',
        'mensagem'
    ];

    function obterStatus(){
        return [
            0 => 'PRONTO PARA ENVIO' , 
            1 => 'ENVIADO PARA DISPARO' , 
            2 => 'AGUARDANDO CONFIRMAÇÃO' , 
            3 => 'ENTREGUE A OPERADORA' , 
            4 => 'NÃO RECEBIDO' , 
            5 => 'DESTINATARIO ENVIOU RESPOSTA' , 
            10 => 'SEM MENSAGEM' , 
            12 => 'MENSAGEM MUITO LONGA' , 
            13 => 'DESTINATÁRIO INVALIDO' , 
            14 => 'CELULAR NÃO INFORMADO' , 
            15 => 'AGENDAMENTO INVÁLIDO' , 
            16 => 'ID COM MAIS DE 50 CARACTERES' , 
            17 => 'TELEFONE OCUPADO' , 
            18 => 'TELEFONE NÃO ATENDE' , 
            19 => 'LIGAÇÃO ATENDIDA',
            20 => 'LIGAÇÃO ATENDIDA MENSAGEM ENTREGUE',
            80 => 'ID JÁ FOI UTILUZADO NAS ULTIMAS 24 HORAS' , 
            900 => 'LOGIN INVALIDO' , 
            990 => 'LIMIT DE CREDITO ATINGIDO' , 
            999 => 'ERRO DESCONHECIDO' , 
            70 => 'ERRO' , 
            71 => 'REJEITADO' , 
            90 => 'ENTREGUE AO DESTINATARIO' , 
        ];
    }
}
