<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Destinatario extends Model
{
    use SoftDeletes;

    protected $fillable =[
        'celular',
        'id_terceiros',
        'status',
        'telefone',
        'operadora',
        'enviado',
        'acesso',
        'mensagem',
        'tipo',
        'cliente_id',
    ];

    protected $casts = [
        'enviado'=> 'datetime',
    ];

    function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    function  status(){
        return $this->belongsTo(Status::class, 'status' , 'codigo_id');
    }
}
