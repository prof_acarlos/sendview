@extends('layouts.app-admin')
@php 
    $hoje = \Carbon\Carbon::now()->locale('pt_BR');
@endphp

@section('content')

<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard
					<small>campanhas</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Dashboard</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <!-- Small boxes (Stat box) -->
                <!-- Info boxes -->
                <div class="row">
                    
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        
                        <div class="info-box">
                            <a href="{{route('ura.ligacoes')}}"> <span class="info-box-icon bg-aqua"><i class="fas fa-tty"></i></span></a>
                            <div class="info-box-content">
                                <span class="info-box-text">Ligações</span>
                                <span class="info-box-number">{{$totais['ligacoes']}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                    <a href="{{route('ura.atendidas')}}"><span class="info-box-icon bg-yellow"><i class="fas fa-phone-volume"></i></span></a>
                                <div class="info-box-content">
                                    <span class="info-box-text">Atendidas</span>
                                    <span class="info-box-number">{{$totais['entregues']}}<small> </small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                
                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <a href="{{route('ura.completa')}}"><span class="info-box-icon bg-green"><i class="fas fa-volume-up"></i></span></a>
                
                            <div class="info-box-content">
                                <span class="info-box-text">Ouviram</span>
                                <span class="info-box-number">{{$totais['completa']}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <a href="{{route('ura.parcial')}}"><span class="info-box-icon bg-red"><i class="fas fa-volume-down"></i></span></a>
                
                            <div class="info-box-content">
                                <span class="info-box-text">Ouviram Parcial</span>
                                <span class="info-box-number">{{$totais['parcial']}}<small></small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fas fa-tty"></i> URA</h3>
                
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="chart-responsive">
                                    <canvas id="pieChart" height="150"></canvas>
                                </div>
                                <!-- ./chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <ul class="chart-legend clearfix">
                                    <li><i class="fas fa-circle-notch text-aqua"></i> Ligações</li>
                                    <li><i class="fas fa-circle-notch text-yellow"></i> Atendidas</li>
                                    <li><i class="fas fa-circle-notch text-green"></i> Ouvidas</li>
                                    <li><i class="fas fa-circle-notch text-red"></i> Ouvidas Parcial</li>
                                    <li><i class="fas fa-circle-notch text-light-blue"></i> Ocupado</li>
                                    <li><i class="fas fa-circle-notch text-gray"></i> Não Atendida</li>
                                </ul>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Ligações realizadas<span class="pull-right text-aqua"><i class="fa fa-angle-up"></i> {!! $totais['ligacoes'] > 0 ? 100 : 0!!}%</span></a></li>
                            <li><a href="#">Ligações atendidas<span class="pull-right text-yellow"><i class="fa fa-angle-up"></i> {{ porcentagem($totais['entregues'], $totais['ligacoes'] ) }}%</span></a></li>
                            <li><a href="#">Ligações ouvidas<span class="pull-right text-green"><i class="fa fa-angle-up"></i> {{ porcentagem($totais['completa'], $totais['ligacoes']) }}%</span></a></li>
                            <li><a href="#">Ligações ouvidas Parcial<span class="pull-right text-red"><i class="fa fa-angle-up"></i> {{ porcentagem( $totais['parcial'], $totais['ligacoes'] ) }}%</span></a></li>
                            <li><a href="#">Ligações ocupado<span class="pull-right text-light-blue"><i class="fa fa-angle-down"></i> {{ porcentagem($totais['ocupados'], $totais['ligacoes']) }}%</span></a></li>
                            <li><a href="#">Ligações não atendidas<span class="pull-right text-gray"><i class="fa fa-angle-down"></i> {{ porcentagem($totais['naoAtendidos'],$totais['ligacoes']) }}%</span></a></li>
                        </ul>
                    </div>
                    <!-- /.footer -->
                </div>
                <!-- /.box -->

               

			</section>
			<!-- /.content -->
		</div>
        <!-- /.content-wrapper -->
        
@endsection
@section('script-js')

<script>
        $(function () {

            'use strict';

            
            // -------------
            // - PIE CHART -
            // -------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
            var pieChart       = new Chart(pieChartCanvas);
            var PieData        = [
                {
                value    : {!! $totais['parcial'] !!},
                color    : '#f56954',
                highlight: '#f56954',
                label    : 'ouvidas parcial'
                },
                {
                value    : {!!$totais['completa'] !!},
                color    : '#00a65a',
                highlight: '#00a65a',
                label    : 'ouvidas'
                },
                {
                value    : {!! $totais['entregues'] !!},
                color    : '#f39c12',
                highlight: '#f39c12',
                label    : 'atendidas'
                },
                {
                value    : {!! $totais['ligacoes'] !!},
                color    : '#00c0ef',
                highlight: '#00c0ef',
                label    : 'realizadas'
                },
                {
                value    : {!! $totais['ocupados'] !!},
                color    : '#3c8dbc',
                highlight: '#3c8dbc',
                label    : 'ocupados'
                },
                {
                value    : {!! $totais['naoAtendidos'] !!},
                color    : '#d2d6de',
                highlight: '#d2d6de',
                label    : 'não atendidas'
                }
            ];
            var pieOptions     = {
                // Boolean - Whether we should show a stroke on each segment
                segmentShowStroke    : true,
                // String - The colour of each segment stroke
                segmentStrokeColor   : '#fff',
                // Number - The width of each segment stroke
                segmentStrokeWidth   : 1,
                // Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                // Number - Amount of animation steps
                animationSteps       : 100,
                // String - Animation easing effect
                animationEasing      : 'easeOutBounce',
                // Boolean - Whether we animate the rotation of the Doughnut
                animateRotate        : true,
                // Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale         : false,
                // Boolean - whether to make the chart responsive to window resizing
                responsive           : true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio  : false,
                // String - A legend template
                legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
                // String - A tooltip template
                tooltipTemplate      : '<%=value %> Ligações  <%=label%> '
            };
            // Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions);
            // -----------------
            // - END PIE CHART -
            // -----------------


        });
</script>
@endsection
