@extends('layouts.app-admin')
@php 
    $hoje = \Carbon\Carbon::now()->locale('pt_BR');
@endphp

@section('content')

<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" >
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					SMS
					<small>extrato </small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class=""><a href="{{route('sms.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li class="active">SMS - Extrato</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <!-- Small boxes (Stat box) -->
                <!-- Info boxes -->
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">SMS - Extrato</h3>
                            <button id="noprint" class="btn btn-default btn-sm pull-right"  onClick="window.print()"><i class="fas fa-print"></i></button>
                        </div>
                        <div class="box-body">
                            
                            @foreach ($extrato as $key => $e)
                                @if($key == 'Entregues')
                                    @continue;
                                @endif
                                <p style="border-bottom: 1px dashed #CCC;">{{$key}} <span class="pull-right">{{$e}}</span></p>
                            @endforeach
                        
                        </div>
                    </div>
                    <!-- /.box -->
                    
                </div>
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="box ">
                        <div class="box-header with-border">
                            <h3 class="box-title">Total - para pagamento</h3>
                        </div>
                        <div class="box-body">
                            <p style="border-bottom: 1px dashed #CCC;">{{__('SMS Enviados')}} <span class="pull-right">{{$extrato['Entregues']}}</span></p>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->

			</section>
			<!-- /.content -->
		</div>
        <!-- /.content-wrapper -->
        
@endsection
