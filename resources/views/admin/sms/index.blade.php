@extends('layouts.app-admin')
@php 
    $hoje = \Carbon\Carbon::now()->locale('pt_BR');
@endphp

@section('content')

<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Dashboard
					<small>campanhas</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Dashboard</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <!-- Small boxes (Stat box) -->
                <!-- Info boxes -->
                <div class="row">
                    
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        
                        <div class="info-box">
                            <a href="{{route('sms.enviados')}}"> <span class="info-box-icon bg-aqua"><i class="fas fa-mobile-alt"></i></span></a>
                            <div class="info-box-content">
                                <span class="info-box-text">envios</span>
                                <span class="info-box-number">{{$totais['enviados']}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                    <a href="{{route('sms.entregues')}}"><span class="info-box-icon bg-yellow"><i class="fas fa-thumbs-up"></i></span></a>
                                <div class="info-box-content">
                                    <span class="info-box-text">Entregues</span>
                                    <span class="info-box-number">{{$totais['entregues']}}<small> </small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                
                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>
                
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <a href="{{route('sms.cliques')}}"><span class="info-box-icon bg-green"><i class="fas fa-hand-point-up"></i></span></a>
                
                            <div class="info-box-content">
                                <span class="info-box-text">Cliques</span>
                                <span class="info-box-number">{{$totais['acessos']}}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <a href="{{route('sms.respostas')}}"><span class="info-box-icon bg-red"><i class="fas fa-reply-all"></i></span></a>
                
                            <div class="info-box-content">
                                <span class="info-box-text">respostas</span>
                                <span class="info-box-number">{{$totais['respostas']}}<small></small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fas fa-mobile-alt"></i> SMS</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text-center">
                                            <strong>Envios: {{$hoje->startOfMonth()->day}} {{$hoje->format('M')}}, {{$hoje->format('Y')}} - {{$hoje->endOfMonth()->day}} {{$hoje->format('M')}}, {{$hoje->format('Y')}}</strong>
                                        </p>
                
                                        <div class="chart">
                                            <!-- Sales Chart Canvas -->
                                            <canvas id="salesChart" style="height: 180px;"></canvas>
                                        </div>
                                        <!-- /.chart-responsive -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-4">
                                        <p class="text-center">
                                            <strong>Resultados</strong>
                                        </p>
                
                                        <div class="progress-group">
                                            <span class="progress-text">Enviados</span>
                                            <span class="progress-number"><b>{{$totais['enviados']}}</b>/{{$totais['enviados']}}</span>
                
                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-aqua" style="width: {{$totais['enviados'] > 0 ? 100 : 0}}%"></div>
                                            </div>
                                        </div>
                                        <!-- /.progress-group -->
                                        <div class="progress-group">
                                            <span class="progress-text">Entregues</span>
                                            <span class="progress-number"><b>{{$totais['entregues']}}</b>/{{$totais['enviados']}}</span>
                
                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-yellow" style="width: {!! porcentagem($totais['entregues'], $totais['enviados']) !!}%"></div>
                                            </div>
                                        </div>
                                        <!-- /.progress-group -->
                                        <div class="progress-group">
                                            <span class="progress-text">Cliques</span>
                                            <span class="progress-number"><b>{{$totais['acessos']}}</b>/{{$totais['entregues']}}</span>
                
                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-green" style="width: {!! porcentagem($totais['acessos'], $totais['entregues'])!!}%"></div>
                                            </div>
                                        </div>
                                        <!-- /.progress-group -->
                                        <div class="progress-group">
                                            <span class="progress-text">Respostas</span>
                                            <span class="progress-number"><b>{{$totais['respostas']}}</b>/{{$totais['acessos']}}</span>
                
                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-red" style="width: {!! porcentagem( $totais['respostas'], $totais['acessos']) !!}%"></div>
                                            </div>
                                        </div>
                                        <!-- /.progress-group -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- ./box-body -->
                            <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block border-right">
                                            <span class="description-percentage text-aqua"><i class="fa fa-caret-up"></i> {{$totais['enviados'] > 0 ? 100 : 0}}%</span>
                                            <h5 class="description-header">{{$totais['enviados']}}</h5>
                                            <span class="description-text">TOTAL ENVIADO</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block border-right">
                                            <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> {!!  porcentagem($totais['entregues'], $totais['enviados'])  !!}%</span>
                                            <h5 class="description-header">{{$totais['entregues']}}</h5>
                                            <span class="description-text">TOTAL ENTREGUE</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block border-right">
                                            <span class="description-percentage text-green"><i class="fa fa-caret-right"></i>  {!! porcentagem($totais['acessos'], $totais['entregues']) !!}%</span>
                                            <h5 class="description-header">{{$totais['acessos']}}</h5>
                                            <span class="description-text">TOTAL CLIQUE</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block">
                                            <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> {!! porcentagem($totais['respostas'], $totais['acessos']) !!}%</span>
                                            <h5 class="description-header">{{$totais['respostas']}}</h5>
                                            <span class="description-text">TOTAL RESPOSTAS</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

			</section>
			<!-- /.content -->
		</div>
        <!-- /.content-wrapper -->
        
@endsection
@section('script-js')

<script>
        $(function () {

            'use strict';

            /* ChartJS
             * -------
             * Here we will create a few charts using ChartJS
             */

            // -----------------------
            // - MONTHLY SALES CHART -
            // -----------------------

            // Get context with jQuery - using jQuery's .get() method.
            var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
            // This will get the first returned node in the jQuery collection.
            var salesChart       = new Chart(salesChartCanvas);

    
            var salesChartData = {
				labels: [{!! $totais['grafico']['enviados']['label'] !!}],
				datasets: [
                    {
						label: 'Enviados',
						fillColor: 'rgb(0,192,239)',
						strokeColor: 'rgb(255,255,255)',
						pointColor: 'rgb(0,192,239)',
						pointStrokeColor: 'rgba(60,141,188,1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(60,141,188,1)',
						data: [{!! $totais['grafico']['enviados']['total'] !!}]
                    },
                    {
						label: 'Entregues',
						fillColor: 'rgb(243,156,18)',
						strokeColor: 'rgb(255,255,255)',
						pointColor: 'rgb(243,156,18)',
						pointStrokeColor: 'rgba(60,141,188,1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(60,141,188,1)',
						data: [{!! $totais['grafico']['entregues']['total'] !!}]
                    },
					{
						label: 'Cliques',
						fillColor: 'rgb(0,166,90)',
						strokeColor: 'rgb(255, 255, 255)',
						pointColor: 'rgb(0,166,90)',
						pointStrokeColor: 'rgb(0,166,90)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgb(220,220,220)',
						data: [{!! $totais['grafico']['cliques']['total'] !!}]
					},
                    {
						label: 'Respostas',
						fillColor: 'rgb(221,75,57)',
						strokeColor: 'rgb(255,255,255)',
						pointColor: 'rgb(221,75,57)',
						pointStrokeColor: 'rgba(60,141,188,1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(60,141,188,1)',
						data: [{!! $totais['grafico']['respostas']['total'] !!}]
					
                    }
                    
				]
			};

            var salesChartOptions = {
				// Boolean - If we should show the scale at all
				showScale: true,
				// Boolean - Whether grid lines are shown across the chart
				scaleShowGridLines: false,
				// String - Colour of the grid lines
				scaleGridLineColor: 'rgba(0,0,0,.05)',
				// Number - Width of the grid lines
				scaleGridLineWidth: 1,
				// Boolean - Whether to show horizontal lines (except X axis)
				scaleShowHorizontalLines: true,
				// Boolean - Whether to show vertical lines (except Y axis)
				scaleShowVerticalLines: true,
				// Boolean - Whether the line is curved between points
				bezierCurve: true,
				// Number - Tension of the bezier curve between points
				bezierCurveTension: 0.3,
				// Boolean - Whether to show a dot for each point
				pointDot: false,
				// Number - Radius of each point dot in pixels
				pointDotRadius: 4,
				// Number - Pixel width of point dot stroke
				pointDotStrokeWidth: 1,
				// Number - amount extra to add to the radius to cater for hit detection outside the drawn point
				pointHitDetectionRadius: 20,
				// Boolean - Whether to show a stroke for datasets
				datasetStroke: true,
				// Number - Pixel width of dataset stroke
				datasetStrokeWidth: 2,
				// Boolean - Whether to fill the dataset with a color
				datasetFill: true,
				// String - A legend template
				legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
				// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
				maintainAspectRatio: true,
				// Boolean - whether to make the chart responsive to window resizing
				responsive: true
			};

            // Create the line chart
            salesChart.Line(salesChartData, salesChartOptions);

            // ---------------------------
            // - END MONTHLY SALES CHART -
            // ---------------------------

        });
</script>
@endsection
