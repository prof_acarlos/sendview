@extends('layouts.app-admin')
@php 
    $hoje = \Carbon\Carbon::now()->locale('pt_BR');
@endphp

@section('content')

<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="height:4100px;">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					SMS
					<small>entregues </small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class=""><a href="{{route('sms.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					<li class="active">SMS - Entregues</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
                <!-- Small boxes (Stat box) -->
                <!-- Info boxes -->
                <!-- /.col -->
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Entregues - {{$objetos->total()}}</h3>
                            <div class="box-tools">
                                <form action="{{route('sms.buscar.entregues')}}" method="POST">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar">
                                        @csrf
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Número</th>
                                    <th>Entregue</th>
                                    <th style="width: 40px">Operadora</th>
                                </tr>
                                @foreach($objetos as $model)
                                    <tr>
                                        <td>{{$model->id}}.</td>
                                        <td>{{mask($model->celular,'(##) ####X-XX## ')}}</td>
                                        <td>{{$model->enviado->format('d/m/Y  \à\s H:i:s ' ?? '')}}</td>
                                        <td><span class="badge bg-aqua">{{$model->operadora ?? 'INDISPONÍVEL'}}</span></td>
                                    </tr>
                                @endforeach
                                
                            </table>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix pull-right">
                                {{$objetos->links()}}
                            </div>
                        </div>
                        @if($objetos->total() < 1)
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning"></i>Ops!</h4>
                            Não encontramos nenhum celular com este número.
                        </div>
                        @endif
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->

			</section>
			<!-- /.content -->
		</div>
        <!-- /.content-wrapper -->
        
@endsection
