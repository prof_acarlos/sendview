
<!DOCTYPE html>
<html>

<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{!! config('app.name', 'Laravel') !!}</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="{!! asset('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}">

	<!-- Font Awesome -->
	<!-- <link rel="stylesheet" href="{!! asset('bower_components/font-awesome/css/font-awesome.min.css') !!}"> -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{!! asset('bower_components/Ionicons/css/ionicons.min.css') !!}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{!! asset('dist/css/AdminLTE.min.css') !!}">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{!! asset('dist/css/skins/_all-skins.min.css') !!}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Google Font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<style>
		.main-header .sidebar-toggle:before {
			content: none!important;
		}
		@media print { 
			#noprint { display:none; } 
			body { background: #fff; }
		}
	</style>
	
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="{{route('home')}}" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>S</b>V</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Send</b>VIEW</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span ><i class="fas fa-bars"></i></span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="{!! asset('dist/img/user2-160x160.jpg') !!}" class="user-image" alt="User Image">
								<span class="hidden-xs">{{\Auth::user()->name ?? 'Usuário'}}</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="{!! asset('dist/img/user2-160x160.jpg') !!}" class="img-circle" alt="User Image">
									<p>
										{{\Auth::user()->name ?? 'Usuário'}}
										<small>Membro desde {{ \Auth::user() ?  \Auth::user()->created_at->format('Y') : \Carbon\Carbon::now()->year }}.</small>
									</p>
								</li>

								<!-- Menu Footer-->
								<li class="user-footer">
									<a href="{{ route('logout') }}" class="btn btn-default btn-block" onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">Sair</a>
								</li>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</ul>
						</li>

					</ul>
				</div>
			</nav>
		</header>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="{!! asset('dist/img/user2-160x160.jpg') !!}" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>{{\Auth::user()->name ?? 'Usuário'}}</p>
						<a href="#"><i class="fas fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MENU PRINCIPAL</li>
					<li class="{{ativo('sms')}} treeview menu-{{ativo('sms')=='ativo'?'open':''}}">
						<a href="#">
							<i class="fa fa-mobile-alt"></i> <span>SMS</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="{{ativo(['dashboard'])}}"><a href="{{route('sms.dashboard')}}"><i class="fa fas fa-circle-notch"></i> Dashboard </a></li>
							<li class="{{ativo(['extrato'])}}"><a href="{{route('sms.extrato')}}"><i class="fa fas fa-circle-notch"></i> Extrato</a></li>
						</ul>
					</li>
					<li class="{{ativo('ura')}} treeview menu-{{ativo('ura')=='ativo'?'open':''}}">
						<a href="#">
							<i class="fas fa fa-tty"></i> <span>URA</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="{{ativo(['dashboard'])}}"><a href="{{route('ura.dashboard')}}"><i class="fas fa fa-circle-notch"></i> Dashboard </a></li>
							<li class="{{ativo(['extrato'])}}"><a href="{{route('ura.extrato')}}"><i class="fas fa fa-circle-notch"></i> Extrato</a></li>
						</ul>
					</li>
					
					{{-- <li><a href="https://adminlte.io/docs"><i class="fas fa fa-book"></i> <span>Documentation</span></a></li> --}}
					<!-- <li class="header">LABELS</li>
					<li><a href="#"><i class="fas fa-circle-notch fa text-red"></i> <span>Important</span></a></li>
					<li><a href="#"><i class="fas fa-circle-notch fa text-yellow"></i> <span>Warning</span></a></li>
					<li><a href="#"><i class="fas fa-circle-notch fa text-aqua"></i> <span>Information</span></a></li> -->
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== -->
		@yield('content')

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.4.0
			</div>
			<strong>Copyright &copy; {{\Carbon\Carbon::now()->year}} <a href="#">Send View</a>.</strong> Todos os direitos reservados.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
	</div>
	<!-- ./wrapper -->


	<!-- jQuery 3 -->
	<script src="{!! asset('bower_components/jquery/dist/jquery.min.js')!!}"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{!! asset('bower_components/bootstrap/dist/js/bootstrap.min.js')!!}"></script>
	<!-- FastClick -->
	<script src="{!! asset('bower_components/fastclick/lib/fastclick.js')!!}"></script>
	<!-- AdminLTE App -->
	<script src="{!! asset('dist/js/adminlte.min.js') !!}"></script>
	<!-- Sparkline -->
	<script src="{!! asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')!!}"></script>
	<!-- SlimScroll -->
	<script src="{!! asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')!!}"></script>
	<!-- ChartJS -->
	<script src="{!! asset('bower_components/chart.js/Chart.js')!!}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

	<!-- AdminLTE for demo purposes -->
	<script src="{!! asset('dist/js/demo.js') !!}"></script>
	<script>
		$(document).ready(function () {
			$('.sidebar-menu').tree()
		})

	</script>
	@yield('script-js')
</body>

</html>